function ArrayHelper(arr = []){
    this.array = arr;
}

ArrayHelper.prototype.getNextId = () => {
    return new Date().valueOf();
    //return Buffer.from(new Date().valueOf().toString()).toString('base64');
    // Buffer.from(b64Encoded, 'base64').toString()
}

ArrayHelper.prototype.deleteFromArray = (arr, id) => {
    arr.splice(arr.findIndex((i) => { 
        return i['id'] === id; 
    }), 1);
    return arr;
}

ArrayHelper.prototype.getObjectById = (arr, id) => {
    return arr.findIndex( (item) => {
        if(parseInt(item['id']) === parseInt(id)){
            return item;
        }
    });
}

module.exports = new ArrayHelper;