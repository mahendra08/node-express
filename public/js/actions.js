$(document).ready(function(){
    if(document.getElementById('btnAddUser')){
        document.getElementById('btnAddUser').addEventListener('click', function(){

            if(document.querySelectorAll('#addUser input:invalid').length || !document.querySelector('#gender').value){
                alert('All fields are mandatory.');
                return false;
            }

            $('#btnAddUser').hide();
            $('#loader').show();
            let formData = {
                fname: $('#fname').val().trim(),
                lname: $('#lname').val().trim(),
                mobile: $('#mobile').val().trim(),
                gender: $('#gender').val().toLowerCase()
            };

            $.ajax({
                type: "POST",
                url: window.location.href,
                data: formData,
                dataType: 'json',
                success:(response) => {
                    setTimeout(() => {
                        location.reload();
                    }, 800);
                }
            });

        });
    }

    if(document.querySelector('form#addUser')){
        document.querySelector('form#addUser').addEventListener("keyup", function(event) {
            if(event.keyCode === 13){
                document.querySelector('#btnAddUser').click();
            }
        });
    }

    if(document.querySelector('#search-text')){
        document.querySelector('#search-text').addEventListener("keyup", function(event) {
            if(event.keyCode === 13){
                document.querySelector('#send-btn').click();
                setTimeout(function(){
                    document.querySelector('#send-btn').setAttribute('class', '');
                }, 60)
                document.querySelector('#send-btn').setAttribute('class', 'onhvr');
                document.querySelector('#search-text').value = '';

                $('#mainDiv').animate({
                    scrollTop: $('#msgBoxContainer').height()
                }, 500);
            }
        });
    }

    if(document.querySelector('input#btnClearFilter')){
        document.querySelector('input#btnClearFilter').addEventListener('click', (event) => {
            window.location = location.pathname;
        });
    }

    $('tbody a.deleteItem').click((event) => {
        let id = $(this.activeElement).data("id");

        let cnfrm = confirm('Are sure, you want to delete this record ?');

        if(!cnfrm){
            return false;
        }

        $.ajax({
            url: `/${id}`,
            method: 'DELETE',
            dataType: 'json',
            success:(response) => {
                setTimeout(() => {
                    location.reload();
                }, 500);
            }
        });
    })
})