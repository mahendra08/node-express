$(document).ready(function(){

    let thisUserInfo;
    let socket = io({transports: ['websocket'], upgrade: false});

    document.addEventListener('visibilitychange', function(){
        // document.visibility.state
        let visibility = (document.hidden) ? ' - Away' : '';
        let title = document.title.split(' - ');
        document.title = title[0].trim()+visibility;
    });

    let intervalCheck = 2000;
    let setIdleTime = 30000;

    function checkUserStatus(){
        idletime+=intervalCheck;
        if(idletime > setIdleTime){
            let title = document.title.split(' - ');
            document.title = title[0].trim()+' - Idle';
            clearInterval(checkIdleStatus);
            intervalStatus = 'inactive';
            socket.emit('status-update', {mobile : mobile, status: 'idle'});
        }
    } 

    let idletime = 0;
    let intervalStatus = 'active';
    let checkIdleStatus = setInterval(checkUserStatus,intervalCheck)

    $('body').on("click keyup", function(){
        idletime = 0;
        let title = document.title.split(' - ');
        document.title = title[0].trim();

        if(intervalStatus=='inactive'){
            socket.emit('status-update', {mobile : mobile, status: 'online'});
            checkIdleStatus = setInterval(checkUserStatus, intervalCheck);
            intervalStatus = 'active';
        }
    });

    socket.on('users-status-update', function(statusDetails){
        usersId = statusDetails.mobile;
        usersStatus = statusDetails.status;

        console.log(usersId, usersStatus);

        let list = document.querySelector(`div#list ul li#u${usersId} div.userContainer`);
        console.log(list);
        list.querySelector(`div[class*='circular_']`).className = `circular_${usersStatus}`;
        list.querySelector(`div.username div.uname`).nextElementSibling.className = usersStatus;
        list.querySelector(`div.username div.status`).innerText = usersStatus;

        if(document.querySelector('div#msgWrapper').style.display!='none'){
            document.querySelector('div#msgWrapper div').className = `msg_circular_${usersStatus}`;
        }

    })

    function scrollToBottom(usrDivId){
        let scrollDiv = $('#mainDiv');
        let newMessage = $(`#usrMsg${usrDivId}`).children('div:last-child')

        let newMessageHeight = newMessage.innerHeight();
        let lastMessageHeight = newMessage.prev().innerHeight();
    
        if(lastMessageHeight !== undefined){
            let clientHeight = scrollDiv.prop('clientHeight');
            let scrollTop = scrollDiv.prop('scrollTop');
            let scrollHeight = scrollDiv.prop('scrollHeight');
        
            let total = clientHeight + scrollTop + newMessageHeight;
            
            console.log(`${clientHeight} + ${scrollTop} + ${newMessageHeight} + ${lastMessageHeight} >= ${scrollHeight}`);
            console.log( total +'>='+ scrollHeight);

            if(clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight){
                scrollDiv.scrollTop(scrollHeight);
                $('#scrollDownBtn').hide();
            }else{
                if(document.querySelector('#scrollDownBtn')){
                    let scrCount = parseInt(document.querySelector('#scrollDownBtn').innerText.trim());
                    document.querySelector('#scrollDownBtn').innerText = scrCount+1;
                }
                $('#scrollDownBtn').show();
            }
        }
    }

    socket.on('connect_error', function() {
        let notifys = [];
        if($(`#notify_${thisUserInfo.mobile}_online`).length==0){
            $('.notification').each(function() {
                if (parseFloat($(this).css('opacity')) > 0) {
                    notifys.push(this);
                }
            });

            let bottom = parseFloat($(notifys[notifys.length-1]).css('bottom'));
                
                if(notifys.length > 0){
                    let lastNotifyBottom = parseFloat($(notifys[notifys.length - 1]).css('bottom'));
                    let lastNotifyHeight = parseFloat($(notifys[notifys.length - 1]).css('height'));
                    let padding = parseFloat($(notifys[notifys.length - 1]).css('padding'));
                    
                    bottom = (lastNotifyBottom + lastNotifyHeight + padding + 5);
                    console.log(`${lastNotifyBottom} + ${lastNotifyHeight} + ${padding} + 5 = ${bottom}`);
                }

                bottom +='px'; 

                let template =`
                <div id="notify_${thisUserInfo.mobile}_online" data.index="${notifys.length}" class="notification red" style="bottom: ${bottom}">
                    Failed to connect to server, retrying...
                </div>`;
                $('body').append(template);
                $(`#notify_${thisUserInfo.mobile}_online`).animate({right:"0%", opacity: "1"}, 500);

                $(`#notify_${thisUserInfo.mobile}_online`).click(function(){
                    $(this).animate({"right":"-21%", "opacity": "0"}, 500, function(){
                        $(this).css('display', 'none');
                    })
                });
            }
    });

    socket.on('connect', function(){

        if($(`#notify_${mobile}_online`).length > 0){
            $(`#notify_${mobile}_online`).addClass('green');
            $(`#notify_${mobile}_online`).removeClass('red');
            $(`#notify_${mobile}_online`).html('Connected to the server..');
            $(`#notify_${mobile}_online`).css('display', ' block');

            $(`#notify_${thisUserInfo.mobile}_online`).animate({"right":"0", "opacity": "1"}, 500, function(){
                setTimeout(function(){
                    $(`#notify_${thisUserInfo.mobile}_online`).animate({"right":"-21%", "opacity": "0"}, 500, function(){
                        $(`#notify_${mobile}_online`).remove();
                    })
                }, 4000)
            })
        }

        socket.emit('newUserConnect', {mobile:mobile}, function(userData){
            thisUserInfo = userData.currUser;
            let userInfo = userData.usersList;
            let avatar = userData.avatar;

            let onlineUsers = userData.online;
            
            let vLi = $('#list li:not([style="display: none;"])');
            if(vLi.length > 0){ $(vLi).remove(); }

            let list = document.querySelector('#list ul');
            if(list.querySelector('li')){
                list.querySelector('li').style.display = 'block';
            }
            
            let list_template = '';
            userInfo.forEach(function(user){
                if(user.mobile == mobile){ return; }
                let userAvatar = '';
                if(user.gender=='female'){
                    let id = Math.floor(Math.random() * avatar.female_avatar.length);
                    userAvatar = 'female/'+avatar.female_avatar[id];
                    avatar.female_avatar.splice(id, 1);
                }else{
                    let id = Math.floor(Math.random() * avatar.male_avatar.length);
                    userAvatar = 'male/'+avatar.male_avatar[id];
                    avatar.male_avatar.splice(id, 1);
                }

                let status = (onlineUsers.indexOf(user.mobile) > -0) ? 'online' : 'offline';

                let li = list.innerHTML;
                li = li.replace(/\[\[STATUS\]\]/g, status);
                li = li.replace('[[NAME]]', `${user.fname} ${user.lname}`);
                li = li.replace('[[AVATAR]]', userAvatar);
                li = li.replace(/\[\[ID\]\]/g, user.mobile);
                list_template += li;

                let usrMsgDiv = `<div id="usrMsg${user.mobile}" style="display: none;"></div>`;
                $('#message-wrapper').append(usrMsgDiv);

            });
            
            
            if(list.querySelector('li')){
                list.querySelector('li').style.display = 'none';
            }
            
            $('#list ul').append(list_template);
            $('#list ul li').click(function(){
                let username = this.querySelector('div.username div').innerText.trim();
                let status = this.querySelector('div.status').innerText.toLowerCase().trim();
                status = `msg_circular_${status}`;
                let userImg = this.querySelector('img').src;

                let liUserMobile = this.id.replace('u', '');

                document.querySelector('#msgWrapper').dataset.mobile = liUserMobile;
                document.querySelector('#msgWrapper').children[0].className = status;
                document.querySelector('#msgWrapper div.msg_username').innerText = username;
                document.querySelector('#msgWrapper img').src = userImg;
                document.querySelector('#msgWrapper').style.display = '';

                document.querySelector('#search-text').disabled = false;
                this.querySelector((`.unread-count`)).innerText = 0;
                this.querySelector((`.unread-count`)).style.display = 'none';

                if(document.querySelector(`#message-wrapper div[style*="display: block"]`)){
                    document.querySelector(`#message-wrapper div[style*="display: block"]`).style.display = ' none';
                }

                document.querySelector(`#usrMsg${liUserMobile}`).style.display = ' block';
                $('#mainDiv').scrollTop($('#msgBoxContainer').height());

            });
        });
    });

    socket.on('userStatusUpdate', statusMsg => {
        let newConnectedUser = (statusMsg.currUser) ? statusMsg.currUser : statusMsg.disConUser;

        if(document.querySelector(`#list ul li#u${newConnectedUser.mobile} .userContainer div[class*='circular_']`)){
            document.querySelector(`#list ul li#u${newConnectedUser.mobile} .userContainer div[class*='circular_']`).className = `circular_${statusMsg.userStatus}`;
            document.querySelectorAll(`#list ul li#u${newConnectedUser.mobile} .userContainer .username div`)[1].className = statusMsg.userStatus;
            document.querySelectorAll(`#list ul li#u${newConnectedUser.mobile} .userContainer .username div`)[2].innerText = statusMsg.userStatus;

            if(document.querySelector('div#msgWrapper div[class*="msg_circular_o"]')){
                document.querySelector('div#msgWrapper div[class*="msg_circular_o"]').className = `msg_circular_${statusMsg.userStatus}`
            }
        }else{

            let avtImage = (newConnectedUser.gender == 'female') ? 'female-avatar-0.png' : 'male-avatar-10.png';
            let list = document.querySelectorAll('#list ul li')[0];
            list.id = `u${newConnectedUser.mobile}`;
            list.style.display = ' block';
            let li = list.innerHTML;
            li = li.replace(/\[\[STATUS\]\]/g, statusMsg.userStatus);    
            li = li.replace('[[NAME]]', `${newConnectedUser.fname} ${newConnectedUser.lname}`);
            li = li.replace('[[AVATAR]]', `${newConnectedUser.gender}/${avtImage}`);
            li = li.replace(/\[\[ID\]\]/g, newConnectedUser.mobile);

            list.innerHTML = li;
            $('#list ul').append(list);
        }

        if(statusMsg.type=='notification'){

            var notifys = [];
            
            $('.notification').each(function() {
                if (parseFloat($(this).css('opacity')) > 0) {
                    notifys.push(this);
                }
            });

            let bottom = parseFloat($(notifys[notifys.length-1]).css('bottom'));

            if(notifys.length > 0){
                let lastNotifyBottom = parseFloat($(notifys[notifys.length - 1]).css('bottom'));
                let lastNotifyHeight = parseFloat($(notifys[notifys.length - 1]).css('height'));
                let padding = parseFloat($(notifys[notifys.length - 1]).css('padding'));
                
                bottom = (lastNotifyBottom + lastNotifyHeight + padding + 5);
                console.log(`${lastNotifyBottom} + ${lastNotifyHeight} + ${padding} + 5 = ${bottom}`);
            }

            bottom +='px'; 

            let template =`
            <div id="notify_${newConnectedUser.mobile}_${statusMsg.userStatus}" data.index="${notifys.length}" class="notification ${statusMsg.class}" style="bottom: ${bottom}">
                ${statusMsg.message}
            </div>`;

            $('body').append(template);

            $(`#notify_${newConnectedUser.mobile}_${statusMsg.userStatus}`).animate({right:"0%", opacity: "1"}, 500, function(){
                setTimeout(function(){
                    $(`#notify_${newConnectedUser.mobile}_${statusMsg.userStatus}`).animate({"right":"-21%", "opacity": "0"}, 500, function(){
                        let thisIndex = document.querySelector(`#notify_${newConnectedUser.mobile}_${statusMsg.userStatus}`).dataset.index;
                        $(`#notify_${newConnectedUser.mobile}_${statusMsg.userStatus}`).remove();
                        notifys.splice(thisIndex, 1);
                })}, 4000)
            });
        }
    });

    socket.on('serverDisconnected', function(msg){
        console.log(msg);
    })
    
/*
    socket.on('disconnect', function(msg){
        let template =`
            <div id="notify_" class="notification ${msg.status}">
                ${msg.message}
            </div>`;

        $('body').append(template);

        $(`#notify_`).animate({right:"0%", opacity: "1"}, 500, function(){
            setTimeout(function(){
                $(`#notify_`).animate({"right":"-21%", "opacity": "0"}, 500), function(){
                    $(`.notification`).remove();
                }}, 4000)
        });
    });
*/
    socket.on('new-message', function(msg){
        sender = msg.sendersMobile;
        console.log('Sender: ', sender);
        console.log('Wrapper: ', document.querySelector('#msgWrapper').dataset.mobile.trim());

        if(document.querySelector('#msgWrapper').dataset.mobile.trim() != sender){
            let count = parseInt(document.querySelector((`#cnt${sender}`)).innerText);
            count++;
            if(count > 0){
                if(count >= 100 && count < 1000){
                    count = parseInt(count/100);
                    count =`${count}00+`;
                } else if(count >= 1000){
                    count = parseInt(count/1000);
                    count = `${count}k+`;
                }
                document.querySelector((`#cnt${sender}`)).innerText = count;
                document.querySelector((`#cnt${sender}`)).style.display = 'block';
            }
        }else{
            scrollToBottom(sender);
        }

        if(!document.querySelector(`#usrMsg${sender}`)){
            $('#message-wrapper').append(`<div id="usrMsg${sender}" style="display: none;"></div>`);
        }

        $(`#usrMsg${sender}`).append(msg.template);
    });

    document.getElementById('send-btn').addEventListener('click', () => {

        if($('#search-text').val().trim()==''){ return false; }

        let message = document.querySelector('#search-text').value.trim();
        let receiversMobile = document.querySelector('#msgWrapper').dataset.mobile;

        document.querySelector('#search-text').value = '';
        let msg = {receiversMobile: receiversMobile, sendersMobile: thisUserInfo.mobile, fname: thisUserInfo.fname, message: message, created_at: new Date().toLocaleString()};
        socket.emit('create-message',msg, function(ack){
            console.log(ack);
        });

        content = template.replace('[[MESSAGE]]', msg.message);
        content = content.replace('[[SENDER]]', thisUserInfo.fname);
        content = content.replace('[[CREATEDAT]]', msg.created_at);
        $(`#usrMsg${receiversMobile}`).append(content);
    });

    
    /*window.onbeforeunload = function (event) {
        var message = 'Important: Please click on \'Save\' button to leave this page.';
        if (typeof event == 'undefined') {
            event = window.event;
        }
        if (event) {
            event.returnValue = message;
        }
        return message;
    };*/

    if(document.querySelector('#search-text')){
        document.querySelector('#search-text').addEventListener("keyup", function(event) {
            if(event.keyCode === 13){
                document.querySelector('#send-btn').click();
                setTimeout(function(){
                    document.querySelector('#send-btn').setAttribute('class', '');
                }, 30)
                document.querySelector('#send-btn').setAttribute('class', 'onhvr');
                document.querySelector('#search-text').value = '';

                $('#mainDiv').animate({
                    scrollTop: $('#msgBoxContainer').height()
                }, 500);
            }
        });
    }


    if(document.querySelector('#scrollDownBtn')){
        document.querySelector('#scrollDownBtn').addEventListener("click", function(event) {
            $('#mainDiv').animate({
                scrollTop: $('#msgBoxContainer').height()
            }, 500, function(){
                document.querySelector('#scrollDownBtn').innerText = 0;
                $('#scrollDownBtn').hide();
            });
        });
    }

    if(document.querySelector('#search-text')){
        document.querySelector('#search-text').addEventListener("keyup", function(event) {
            if(event.keyCode === 13){
                document.querySelector('#send-btn').click();
            }
        });
    }
});