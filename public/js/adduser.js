$(document).ready(function(){
    document.getElementById('btnAddUser').addEventListener('click', function(){

        $('#btnSubmit').hide();
        $('#loader').show();
        let formData = {
            fname: $('#fname').val().trim(),
            lname: $('#lname').val().trim()
        };

        $.ajax({
            type: "POST",
            url: window.location.href,
            data: formData,
            dataType: 'json',
            success:(response) => {
                setTimeout(() => {
                    location.reload();
                }, 800);
            }
        });

    });

    document.querySelector('form#addUser input[type="text"]').addEventListener("keyup", (event) => {
        if(event.keycode === 13){
            document.querySelector('#btnAddUser').click();
        }
    });

})