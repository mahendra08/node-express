const socketIo = require('socket.io');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const _ = require('underscore');

let app = express();

const server = http.createServer(app);
const sio = socketIo(server);
const arrayHelper = require('./array-helper');


let port = process.env.PORT || 1443;
let fileName = `${__dirname}/users/files/list.json`;
let onlineUsers = [];
let sockIds = [];

app.use(bodyParser.urlencoded({extended: true}));
app.use('/assets/css/', express.static(`${__dirname}/public/style`));
app.use('/assets/img/', express.static(`${__dirname}/public/images`));
app.use('/assets/javascript/', express.static(`${__dirname}/public/js`));
app.set('view engine', 'ejs');


if(!fs.existsSync(fileName)){
    fs.writeFileSync(fileName, '[]', 'utf8');
}

server.listen(port, () => {
    console.info(`Listening to port ${port}`);
});

app.post('/', (req, res) => {
    let mobile = req.body.mobile;
    fs.readFile(fileName, {encoding: 'utf8'}, (err, data)=>{
        if(err)
            throw err;

        data = (data=='') ? '[]' : data;

        let usersList = JSON.parse(data);

        let userInfo = _.findWhere(usersList, {mobile: mobile});
                
        if(userInfo){
            res.redirect(`/chat/${mobile}`);
        }else{
            res.redirect('/?err=1');
        }
    });
});



let msg = {};

app.get('/', (req, res) => {

    if(req.query.err){
        msg = {status: 'error', message: 'Mobile not available'};
    }else if(req.query.success){
        msg = {status: 'success', message: 'User added successfully..'};
    }else{
        msg = {status: '', message: 'Welcome..'};
    }
    
    res.render('index', {msg:msg});
});

sio.on('connection', (socket) => {

    socket.on('newUserConnect', (info, callback) => {

        let getPerson = (fileName) => {
            return new Promise((resolve, reject) => {
                fs.readFile(fileName, {encoding : 'utf8'}, (err, content) => {
                    (err) ? reject(err) : resolve(content);
                })
            })
        }

        getPerson(fileName).then((content) => {
            let usersList = JSON.parse(content);

            let female_avatar = fs.readdirSync('./public/images/female/');
            let male_avatar = fs.readdirSync('./public/images/male/');

            let online = JSON.stringify(onlineUsers);

            let avatar = { female_avatar: female_avatar, male_avatar: male_avatar};
            let currUser = _.findWhere(usersList, {mobile : info.mobile});
            //let currSockId = _.findWhere(onlineUsers, {mobile : info.mobile});

            onlineUsers.push(info.mobile);
            sockIds.push({fname: currUser.fname, lname: currUser.lname, mobile:info.mobile, sockId: socket.id});

            console.log('Active online users: ', onlineUsers);
            console.log('Active sockers', sockIds);

            socket.broadcast.emit('userStatusUpdate', {type: 'notification', userStatus: 'online', class: 'green', message: `${currUser.fname} ${currUser.lname} is online.`, currUser});
            callback({currUser, usersList, avatar, online});
        });
    });


    socket.on('status-update', (statusDetails) => {
        socket.broadcast.emit('users-status-update', statusDetails);
    });

    socket.on('disconnect', () => {

        let disConSockInfo = _.findWhere(sockIds, {sockId: socket.id});

        if(disConSockInfo !== undefined){

            let disConSock = disConSockInfo.sockId;
            let disConMobile = disConSockInfo.mobile;
            let index = onlineUsers.indexOf(disConMobile);

            if(index > -1){
                let disConUser = _.findWhere(sockIds, { sockId: disConSock});
                sockIds = _.without(sockIds, disConUser);
                onlineUsers.splice(index, 1);

                console.log('disconnected user: ', disConUser);
                console.log('Active users after 1 disconnect: ', sockIds);
                console.log('Active online users after 1 disconnect: ', onlineUsers);
                socket.broadcast.emit('userStatusUpdate', {type: 'notification', userStatus: 'offline', class: 'grey', message: `${disConUser.fname} ${disConUser.lname} went offline.`, disConUser});
            }

        }
    })

    socket.on('create-message', (msg , callback) => {

        sendersMobile = msg.sendersMobile;

        if(onlineUsers.indexOf(msg.receiversMobile) < 0){
            callback({ server: 1, receive: 0, read: 0, message: 'User is offline'});
            return;
        }
        
        let msgTemplate = `${__dirname}/views/received-message.ejs`;

        let getSocketInfo = _.findWhere(sockIds, {mobile : msg.receiversMobile});

        if(getSocketInfo !== undefined){
                fs.readFile(msgTemplate, {encoding : 'utf8'}, (err, template) => {
                msg.created_at = new Date().toLocaleString();
                sender = msg.fname;

                if(err)
                    throw err;
            
                template = template.replace('[[SENDER]]', sender);
                template = template.replace('[[MESSAGE]]', msg.message);
                template = template.replace('[[CREATEDAT]]', msg.created_at);

                socket.broadcast.to(getSocketInfo.sockId).emit('new-message', {template : template, sendersMobile: sendersMobile, receiversMobile: msg.receiversMobile});
                callback({ server: 1, receive: 1, read: 0, message: 'Message sent'});
            });
        }else{
            callback({ server: 1, receive: 0, read: 0, message: 'User is offline'});
        }
    });

});

app.get('/signup', (req, res) => {
    msg = (req.query.err) ? {status: 'error', message: 'Mobile already registered'} : {status: '', message: 'Sign Up'};
    res.render('signup', {msg: msg});
});

app.post('/signup', (req, res) => {
    let params = req.body;
    let mobile = params.mobile;

    fs.readFile(fileName, {encoding: 'utf-8'}, (err, content) =>{

        if(err)
            throw err;

        content = (content=='') ? '[]' : content;
        let usersList = JSON.parse(content);
        
        let user = _.findWhere(usersList, {mobile: mobile});

        if(user!==undefined){
            res.redirect('/signup?err=1;')
        }else{
            let userData = {
                id: arrayHelper.getNextId(),
                fname: params.fname,
                lname: params.lname,
                mobile: params.mobile,
                gender: params.gender,
            }

            usersList.push(userData);
            fs.writeFile(fileName, JSON.stringify(usersList), {encoding: 'utf-8'}, (err) => {

                if(!err){
                    res.redirect('/?success=1');
                }
            });

        }

    })
});

app.get('/chat/:mobile', (req, res) => {
    let msgTemplate = `${__dirname}/views/sent-message.ejs`;
    fs.readFile(msgTemplate, (err, template) => {
        if(err)
            throw err;

        res.render('chat', {template: template, mobile: req.params.mobile});
    });
});

app.get('/person/:id', (req, res) => {

    let getPerson = (fileName) => { 
            return new Promise((resolve, reject) => {
            fs.readFile(fileName, {encoding : 'utf8'}, (err, content) => {
                (err) ? reject(err) : resolve(content);
            })
        })
    }

    getPerson(fileName).then((content) => {
        let filter = _.findWhere(JSON.parse(content), {id : parseInt(req.params.id)});

        let person = (filter) ? filter.fname+' '+filter.lname : '-::- No Record Found -::-';
        res.render('userinfo', {msg:person});
    });
});

app.get('/list', (req, res) => {
    let filter = {};

    if(req.query.lname && req.query.lname!='')
        filter.lname = req.query.lname.trim();
    if(req.query.fname && req.query.fname!='')
        filter.fname = req.query.fname.trim(); 
    if(req.query.mobile && req.query.mobile!='')
        filter.mobile = req.query.mobile.trim(); 
    if(req.query.gender && req.query.gender!='')
        filter.gender = req.query.gender.toLowerCase(); 
    if(req.query.id && req.query.id!='')
        filter.id = req.query.id.trim(); 

    let readContent = (fileName) => {
        return new Promise((resolve, reject) => {
            fs.readFile(fileName, {encoding: 'utf8'}, (err, content) => {
                (err) ? reject(err) : resolve(content);
            })    
        });
    };

    readContent(`${__dirname}/users/files/list.json`).then((usersList) => {    
        usersList = (usersList=='') ? '[]' : usersList;
        let list = JSON.parse(usersList);

        if(filter)
            list = _.where(list, filter);
         else{
             filter = {fname:'', lname:'', id:''}
         }
        res.render('list', {list, filter});
    });

});

app.post('/list', (req, res) => {
    let list = fs.readFileSync(`${__dirname}/users/files/list.json`, {encoding: 'utf8'});
    let frmData = req.body;
    if(frmData.btnSubmit){
        delete frmData.btnSubmit;
    }

    list = (list=='') ? '[]' : list;

    listData = JSON.parse(list);

    frmData['id'] = arrayHelper.getNextId();
    listData.push(frmData);
    
    let writeContent = (fileName) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(fileName, JSON.stringify(listData), {encoding : 'utf8'}, (err, data) => {
                if(err){
                    reject(err);
                    return
                }
                resolve(data);
            });
        });
    };

    writeContent(fileName).then((data) => {
        res.json({status: 'success', data:data});
        res.end();
    });
    
});


app.delete('/:id', (req, res) => {
    let id = req.params.id;
    let list = fs.readFileSync(fileName, {encoding: 'utf8'});
    list = JSON.parse(list);
    let listData = arrayHelper.deleteFromArray(list, id);

    let writeContent = (fileName) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(fileName, JSON.stringify(listData), {encoding : 'utf8'}, (err, data) => {
                if(err){
                    reject(err);
                    return
                }
                resolve(data);
            });
        });
    };

    writeContent(fileName).then((data) => {
        res.json({status: 'success', data:data});
        res.end();
    });
})